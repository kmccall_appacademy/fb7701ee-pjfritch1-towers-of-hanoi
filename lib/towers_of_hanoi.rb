class TowersOfHanoi
  def initialize
    @towers = [[3,2,1],[],[]]
  end

  attr_reader :towers

  def move(from_tower, to_tower)
    disc = towers[from_tower].pop
    towers[to_tower] << disc
  end

  def valid_move?(from_tower, to_tower)
    if towers[from_tower].empty?
    puts "You can't pull from an empty tower, silly!"
      return false
    elsif !towers[to_tower].empty? && towers[to_tower].last < towers[from_tower].last
      puts "Oops! you have to put smaller discs on top of larger ones!"
      return false
    end

    true
  end

  def won?
    return true if towers[1].count == 3 || towers[2].count == 3

    false
  end

  def play
    puts "Welcome to the Towers of Hanoi!"

    until won?
      puts "Which tower would you like to move from?"
      from_tower = gets.chomp.to_i - 1
      puts "Where do you want to put this disk?"
      to_tower = gets.chomp.to_i - 1
      if valid_move?(from_tower,to_tower)
        move(from_tower,to_tower)
        render
      end
    end
  end

  def render
    system("clear")
    puts "Tower 1: #{towers[0]}\nTower 2: #{towers[1]}\nTower 3: #{towers[2]}"
  end

  puts "Congratulations! you moved the tower!"
end
